package com.example.demo;

/**
 * Here is class with lambda expression
 */
public class RunnableLambda {

    public static void main(String[] args) {
        /**
         * Runnable method with lambda expression
         */
        Runnable r1 = () -> {
            System.out.println("Runnable with Lambda Class");
        };
        Runnable r2 = () -> {
            System.out.println("Runnable with Lambda Expression");
        };
        //threads
        new Thread(r1).start();
        new Thread(r2).start();
    }
}