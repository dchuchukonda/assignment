package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DemonThreadTest {
	@Test
    void testdaemon() {
        DaemonThread t1 = new DaemonThread();
        t1.setDaemon(true);
        assertEquals(false, DaemonThread.currentThread().isDaemon());
        assertEquals(true, t1.isDaemon());



   }

}


