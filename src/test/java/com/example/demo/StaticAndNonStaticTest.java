package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class StaticAndNonStaticTest {

	 @Test
	    void nonstatictest() {
		 StaticAndNonStatic obj1 = new StaticAndNonStatic();
		 StaticAndNonStatic obj2 = new StaticAndNonStatic();
		 StaticAndNonStatic obj3 = new StaticAndNonStatic();
	        System.out.println(obj1.nonstaticcount);
	        assertNotEquals(3, obj1.nonstaticcount);



	   }



	   @Test
	    void statictest() {
		   StaticAndNonStatic obj4 = new StaticAndNonStatic();
	        System.out.println(obj4.countstatic);
	        StaticAndNonStatic obj5 = new StaticAndNonStatic();
	        System.out.println(obj5.countstatic);
	        StaticAndNonStatic obj6 = new StaticAndNonStatic();
	        System.out.println(obj6.countstatic);
	        assertEquals(6, obj6.countstatic);



	   }
}
